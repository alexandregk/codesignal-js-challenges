// This function will check if two strings are equal. (normal and reverse) example: ac = ca -> false, aabaa = aabaa -> true
function checkPalindrome(inputString) {
    /* 
        var reverseString -> save the reverse
        inputString       -> save the string by param
        split("")         -> will separate the string into Array
        reverse()         -> the string will be reversed
        join("")          -> joins the characters
    */
    var reverseString = inputString.split("").reverse().join("");
    
    // (if reverseString == inputstring) {return true; } return false;
    return reverseString == inputString ? true : false;
}