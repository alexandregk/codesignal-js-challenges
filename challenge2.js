// Return century from the year by param
function centuryFromYear(year) {
    // Math.ceil() round up
    // Math.round() round down
    return Math.ceil(year / 100) + 1;
}